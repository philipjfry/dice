DICE!

I created this as a learning project for OOP.  
It is probably not the most efficient as I'm still a fairly novice coder.

Usage: dice -d <sides> -a <amount>

I wrote it to help with game night with a few friends.  The primary goal being
that I would add it to /usr/local/bin on my laptop then create aliases for each 
of the common dice used in D&D/Pathfinder/etc.

If I hit a max roll or a min roll, it would tell me with "Critical Hit" or
"Critical Failure"

It worked like a charm!

Feel free to send whatever requests, comments, critiques.

