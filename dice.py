#!/usr/bin/env python3

import random
import argparse

parser = argparse.ArgumentParser(description='Dice roller! Specify the type and amount of dice you want to roll with -d and -a then get your results printed out to the terminal.')
parser.add_argument('-d', '--dice', help='The type of dice you would like to roll.', type=int)
parser.add_argument('-a', '--amount', help='How many of the dice would you like to roll?', type=int)
args = parser.parse_args()

#Handling expectations of the arguments provided
if args.dice is None and args.amount is None:
    parser.print_help()
    exit(0)
elif args.dice is None and args.amount is not None:
    print('No dice type given. Assuming 20-sided die...')
    dice_type = '20'
    amount = str(args.amount)
elif args.dice is not None and args.amount is None:
    dice_type = str(args.dice)
    amount = '1'
elif args.dice is not None and args.amount is not None:
    dice_type = str(args.dice)
    amount = str(args.amount)
else:
    dice_type = '20'
    amount = '1'

class Dice:
    '''
    Instantiating the dice class
    '''
    
    def __init__(self, sides, amount):
        '''
        Init function for dice class
        '''

        self.sides = int(sides) + 1
        self.amount = int(amount)

    def roll(self):
        '''
        Creating the die and returning the result of the roll
        '''

        #Creating a single die and rolling
        outcome = random.randrange(1, self.sides)

        #Returning the result of the single die roll
        return outcome

    def number_of_rolls(self):
        '''
        Rolling the amount of dice specified and returning the results of each roll
        '''

        #Creating empty list for the roll results
        roll_results = []

        #Rolling the dice the amount of times specified
        while self.amount > 0:
            roll_results.append(self.roll())
            self.amount -= 1
        
        #Returning the list of dice roll results
        return roll_results

def  __main__():
    '''
    Creating the dice, rolling them, then returning the results.
    '''

    #Creating the dice
    chance = Dice(dice_type, amount)

    #Rolling the dice and returning the results to terminal.  Will show if a critical failure or critical success.
    for result in chance.number_of_rolls():
        if result == 1:
            print(f'CRITICAL FAILURE!\nYour roll is {result}')
        elif result == (chance.sides - 1):
            print(f'CRITICAL HIT!\nYour roll is {result}')
        else:
            print(f'Your roll is {result}')

#Running through the main function
__main__()

exit(0)
