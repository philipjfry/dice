from dataclasses import dataclass
import argparse
from random import randrange

@dataclass
class Dice:
    '''
    Instantiating Dice class as a dataclass
    '''
    sides: int = 20
    amount: int = 1

    def roll(self) -> int:
        '''
        Returning results of the dice roll
        '''
        return randrange(1, self.sides + 1)

    def roll_results(self) -> list:
        '''
        Returning a list of the results.  Length of list is the amount passed thru the arguments.
        '''
        results = []
        while len(results) < self.amount:
            results.append(self.roll())

        return results


def main():
    '''
    Main function:
    1. Collect argument info
    2. Instantiate Dice class
    3. Return results
    '''

    parser = argparse.ArgumentParser(
        description='Dice roller! Specify the type and amount of dice you want to roll with -d and -a then get your results printed out to the terminal.')
    parser.add_argument('-d', '--dice', help='The type of dice you would like to roll.', type=int, default=20)
    parser.add_argument('-a', '--amount', help='How many of the dice would you like to roll?', type=int, default=1)
    args = parser.parse_args()

    results = Dice(sides=args.dice, amount=args.amount)
    [print(f'Roll: {result}') for result in results.roll_results()]



if __name__ == "__main__":
    main()